from __future__ import annotations

import csv
import logging
import time

from argparse import ArgumentParser
from argparse import Namespace
from logging import getLogger
from pathlib import Path

from akkumulator.database.influx import InfluxWrapper
# from akkumulator.database.sqllite import init_db
from akkumulator.model.power import PowerPerDay
from akkumulator.model.power import PowerPerMonth


_log = getLogger("default")
ch = logging.StreamHandler()
_log.addHandler(ch)

UPDATE_EVERY_SEC = 300


def parse() -> Namespace:
    parser = ArgumentParser(description="System to record the data on a trimodal crane")
    parser.add_argument(
        "-d",
        "--dryrun",
        action="store_const",
        const="dryrun",
        help="don't commit to influxdb",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        const="verbose",
        help="verbose",
    )
    return parser.parse_args()


def power_per_day(db: InfluxWrapper, db_name: str) -> None:
    with Path("/srv/csv/power_per_day.csv").open("w", newline="", encoding="UTF-8") as csvfile:
        writer = csv.writer(
            csvfile,
            delimiter=",",
        )
        total = db.get_day_total(database=db_name, device="ew_strom")
        writer.writerow(PowerPerDay.row_header())
        for day in total:
            writer.writerow(day.row_str())


def power_per_month(db: InfluxWrapper, db_name: str) -> None:
    with Path("/srv/csv/power_per_month.csv").open("w", newline="", encoding="UTF-8") as csvfile:
        writer = csv.writer(
            csvfile,
            delimiter=",",
        )
        per_month = db.power_per_month(database=db_name, device="ew_strom")
        writer.writerow(PowerPerMonth.row_header())
        for month in per_month:
            writer.writerow(per_month[month].row_str())


def log_infos(db: InfluxWrapper):
    db_list = db.get_databases()
    _log.info("influxdb databases: " + str(db.get_databases()))
    for db_name in db_list:
        _log.info(f"influxdb series of {db_name}: {db.get_series(database=db_name)}")
    for db_name in db_list:
        _log.info(f"influxdb tags of {db_name}: {db.get_tags(database=db_name)}")
    all_devices = db.get_devices(database="tasmota")
    for dev in all_devices:
        _log.info(f"influxdb tasmota devices: {dev}")


def core() -> None:
    args = parse()
    _log.setLevel(logging.INFO)
    if args.verbose:
        _log.setLevel(logging.DEBUG)
    influx_db = InfluxWrapper()
    # sqllite_db = init_db(Path("/srv/sqlite/iotakkulumator.db"))

    _log.info("core started")
    log_infos(influx_db)
    while 1:
        power_per_day(influx_db, "tasmota")
        power_per_month(influx_db, "tasmota")
        time.sleep(UPDATE_EVERY_SEC)


if __name__ == "__main__":
    core()
