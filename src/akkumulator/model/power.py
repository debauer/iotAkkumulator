from __future__ import annotations

from dataclasses import dataclass
from dataclasses import field
from datetime import datetime


@dataclass
class PowerPerDay:
    date: datetime
    power: float
    device: str
    euro: float = 0.39

    def nice_date(self) -> str:
        return str(self.date.date()).replace("-", ".")

    def year_month(self) -> str:
        return f"{self.date.year}-{self.date.month}"

    def row_str(self) -> str:
        return [self.date, self.nice_date(), self.power, f"{self.power * self.euro:.3f}"]

    @staticmethod
    def row_header() -> str:
        return ["date", "nicedate", "value", "euro"]


@dataclass
class PowerPerMonth:
    year_month: str
    device: str
    per_day: list[PowerPerDay] = field(init=False)
    euro: float = 0.39

    def __post_init__(self) -> None:
        self.per_day = []

    def add(self, data: PowerPerDay) -> None:
        self.per_day.append(data)

    def row_str(self) -> str:
        year = self.year_month.split("-")[0]
        month = self.year_month.split("-")[1]
        summe = sum([d.power for d in self.per_day])
        return [
            f"{year}-{month.zfill(2)}-01 00:00:00+00:00",
            self.year_month,
            f"{summe:.3f}",
            f"{summe * self.euro:.3f}",
        ]

    @staticmethod
    def row_header() -> str:
        return ["date", "nicedate", "value", "euro"]
