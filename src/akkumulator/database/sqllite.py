import sqlite3
from pathlib import Path
from sqlite3 import Connection
from sqlalchemy as db
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import MappedAsDataclass


class Base(MappedAsDataclass, DeclarativeBase):
    """subclasses will be converted to dataclasses"""


class User(Base):
    __tablename__ = "user_account"

    id: Mapped[int] = mapped_column(init=False, primary_key=True)
    name: Mapped[str]

def init_db(sqllite_path: Path) -> Connection:
    sqllite_exist = sqllite_path.exists()
    engine = db.create_engine("sqlite:///european_database.sqlite")
    conn = engine.connect() 
        
        db = sqlite3.connect("/srv/sqlite/iotakkulumator.db")
        if not sqllite_exist:
            db.execute("CREATE TABLE per_day(device, date, nicedate, value, euro)")
            db.execute("CREATE TABLE per_month(device, date, nicedate, value, euro)")